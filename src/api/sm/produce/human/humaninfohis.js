import request from '@/utils/request'

// 查询人力调整历史列表
export function listHumaninfohis(query) {
  return request({
    url: '/human/humaninfohis/list',
    method: 'get',
    params: query
  })
}

// 查询人力调整历史详细
export function getHumaninfohis(id) {
  return request({
    url: '/human/humaninfohis/' + id,
    method: 'get'
  })
}

// 新增人力调整历史
export function addHumaninfohis(data) {
  return request({
    url: '/human/humaninfohis',
    method: 'post',
    data: data
  })
}

// 修改人力调整历史
export function updateHumaninfohis(data) {
  return request({
    url: '/human/humaninfohis',
    method: 'put',
    data: data
  })
}

// 删除人力调整历史
export function delHumaninfohis(id) {
  return request({
    url: '/human/humaninfohis/' + id,
    method: 'delete'
  })
}
