import request from '@/utils/request'

// 查询人力调整列表
export function listHumaninfo(query) {
  return request({
    url: '/human/humaninfo/list',
    method: 'get',
    params: query
  })
}

// 查询人力调整详细
export function getHumaninfo(id) {
  return request({
    url: '/human/humaninfo/' + id,
    method: 'get'
  })
}

// 新增人力调整
export function addHumaninfo(data) {
  return request({
    url: '/human/humaninfo',
    method: 'post',
    data: data
  })
}

// 修改人力调整
export function updateHumaninfo(data) {
  return request({
    url: '/human/humaninfo',
    method: 'put',
    data: data
  })
}

// 删除人力调整
export function delHumaninfo(id) {
  return request({
    url: '/human/humaninfo/' + id,
    method: 'delete'
  })
}
