import request from '@/utils/request'

// 查询班组日生产数据上报列表
export function listDaysubmit(query) {
  return request({
    url: '/produce/daysubmit/list',
    method: 'get',
    params: query
  })
}

// 查询班组日生产数据上报详细
export function getDaysubmit(id) {
  return request({
    url: '/produce/daysubmit/' + id,
    method: 'get'
  })
}

// 新增班组日生产数据上报
export function addDaysubmit(data) {
  return request({
    url: '/produce/daysubmit',
    method: 'post',
    data: data
  })
}

// 修改班组日生产数据上报
export function updateDaysubmit(data) {
  return request({
    url: '/produce/daysubmit',
    method: 'put',
    data: data
  })
}

// 删除班组日生产数据上报
export function delDaysubmit(id) {
  return request({
    url: '/produce/daysubmit/' + id,
    method: 'delete'
  })
}

export function selectDailyProductionSummary(data) {
  return request({
    url: '/order/statistics/selectDailyProductionSummary',
    method: 'post',
    data: data
  })
}

export function exportDailyProductionSummary(query) {
  return request({
    url: `/order/statistics/exportDailyProductionSummary/${query.planDate}/${query.factoryId}/${query.teamId}`,
    method: 'get'
  })
}



