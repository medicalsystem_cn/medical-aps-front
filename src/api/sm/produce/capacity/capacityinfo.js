import request from '@/utils/request'

// 查询产能调整列表
export function listCapacityinfo(query) {
  return request({
    url: '/capacity/capacityinfo/list',
    method: 'get',
    params: query
  })
}

// 查询产能调整详细
export function getCapacityinfo(id) {
  return request({
    url: '/capacity/capacityinfo/' + id,
    method: 'get'
  })
}

// 新增产能调整
export function addCapacityinfo(data) {
  return request({
    url: '/capacity/capacityinfo',
    method: 'post',
    data: data
  })
}

// 修改产能调整
export function updateCapacityinfo(data) {
  return request({
    url: '/capacity/capacityinfo',
    method: 'put',
    data: data
  })
}

// 删除产能调整
export function delCapacityinfo(id) {
  return request({
    url: '/capacity/capacityinfo/' + id,
    method: 'delete'
  })
}
