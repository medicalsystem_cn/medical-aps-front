import request from '@/utils/request'

// 查询产能调整历史列表
export function listCapacityinfohis(query) {
  return request({
    url: '/capacity/capacityinfohis/list',
    method: 'get',
    params: query
  })
}

// 查询产能调整历史详细
export function getCapacityinfohis(id) {
  return request({
    url: '/capacity/capacityinfohis/' + id,
    method: 'get'
  })
}

// 新增产能调整历史
export function addCapacityinfohis(data) {
  return request({
    url: '/capacity/capacityinfohis',
    method: 'post',
    data: data
  })
}

// 修改产能调整历史
export function updateCapacityinfohis(data) {
  return request({
    url: '/capacity/capacityinfohis',
    method: 'put',
    data: data
  })
}

// 删除产能调整历史
export function delCapacityinfohis(id) {
  return request({
    url: '/capacity/capacityinfohis/' + id,
    method: 'delete'
  })
}
