import request from '@/utils/request'

// 查询站点生产数据上报列表
export function listStationday(query) {
  return request({
    url: '/mobile/stationday/list',
    method: 'get',
    params: query
  })
}

// 查询站点生产数据上报列表
export function listExportStationday(query) {
  return request({
    url: '/mobile/stationday/exportlist',
    method: 'post',
    params: query
  })
}



// 查询站点生产数据上报详细
export function getStationday(id) {
  return request({
    url: '/mobile/stationday/' + id,
    method: 'get'
  })
}

// 新增站点生产数据上报
export function addStationday(data) {
  return request({
    url: '/mobile/stationday',
    method: 'post',
    data: data
  })
}

// 修改站点生产数据上报
export function updateStationday(data) {
  return request({
    url: '/mobile/stationday',
    method: 'put',
    data: data
  })
}

// 删除站点生产数据上报
export function delStationday(id) {
  return request({
    url: '/mobile/stationday/' + id,
    method: 'delete'
  })
}
