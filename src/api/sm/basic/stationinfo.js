import request from '@/utils/request'

// 查询班组站点管理列表
export function listStationinfo(query) {
  return request({
    url: '/basic/stationinfo/list',
    method: 'get',
    params: query
  })
}

// 查询班组站点管理列表
export function listAllStationinfo(query) {
  return request({
    url: '/basic/stationinfo/listAll',
    method: 'get',
    params: query
  })
}

// 查询班组站点管理详细
export function getStationinfo(id) {
  return request({
    url: '/basic/stationinfo/' + id,
    method: 'get'
  })
}

// 新增班组站点管理
export function addStationinfo(data) {
  return request({
    url: '/basic/stationinfo',
    method: 'post',
    data: data
  })
}

// 修改班组站点管理
export function updateStationinfo(data) {
  return request({
    url: '/basic/stationinfo',
    method: 'put',
    data: data
  })
}

// 删除班组站点管理
export function delStationinfo(id) {
  return request({
    url: '/basic/stationinfo/' + id,
    method: 'delete'
  })
}

export function changeStatus(id,status) {
  return request({
    url: '/basic/stationinfo/changeStatus?id=' + id +'&status=' +status,
    method: 'get'
  })
}
