import request from '@/utils/request'

// 查询节假日维护列表
export function listHolidayday(query) {
  return request({
    url: '/basic/holidayday/list',
    method: 'get',
    params: query
  })
}

// 查询节假日维护详细
export function getHolidayday(id) {
  return request({
    url: '/basic/holidayday/' + id,
    method: 'get'
  })
}

// 新增节假日维护
export function addHolidayday(data) {
  return request({
    url: '/basic/holidayday',
    method: 'post',
    data: data
  })
}

// 修改节假日维护
export function updateHolidayday(data) {
  return request({
    url: '/basic/holidayday',
    method: 'put',
    data: data
  })
}

// 删除节假日维护
export function delHolidayday(id) {
  return request({
    url: '/basic/holidayday/' + id,
    method: 'delete'
  })
}
