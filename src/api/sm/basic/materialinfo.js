import request from '@/utils/request'

// 查询物料清单信息列表
export function listMaterialinfo(query) {
  return request({
    url: '/basic/materialinfo/list',
    method: 'get',
    params: query
  })
}

// 查询物料清单信息详细
export function getMaterialinfo(id) {
  return request({
    url: '/basic/materialinfo/' + id,
    method: 'get'
  })
}

// 新增物料清单信息
export function addMaterialinfo(data) {
  return request({
    url: '/basic/materialinfo',
    method: 'post',
    data: data
  })
}

// 修改物料清单信息
export function updateMaterialinfo(data) {
  return request({
    url: '/basic/materialinfo',
    method: 'put',
    data: data
  })
}

// 删除物料清单信息
export function delMaterialinfo(id) {
  return request({
    url: '/basic/materialinfo/' + id,
    method: 'delete'
  })
}

export function listChildren(query) {
  return request({
    url: '/basic/materialinfo/listChildren',
    method: 'get',
    params: query
  })
}

