import request from '@/utils/request'

// 查询产品信息列表
export function listProductinfo(query) {
  return request({
    url: '/basic/productinfo/list',
    method: 'get',
    params: query
  })
}

// 查询产品信息详细
export function getProductinfo(id) {
  return request({
    url: '/basic/productinfo/' + id,
    method: 'get'
  })
}

// 新增产品信息
export function addProductinfo(data) {
  return request({
    url: '/basic/productinfo',
    method: 'post',
    data: data
  })
}

// 修改产品信息
export function updateProductinfo(data) {
  return request({
    url: '/basic/productinfo',
    method: 'put',
    data: data
  })
}

// 删除产品信息
export function delProductinfo(id) {
  return request({
    url: '/basic/productinfo/' + id,
    method: 'delete'
  })
}
