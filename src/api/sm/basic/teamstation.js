import request from '@/utils/request'

// 查询班组配置站点信息列表
export function listTeamstation(query) {
  return request({
    url: '/basic/teamstation/list',
    method: 'get',
    params: query
  })
}

// 查询班组配置站点信息详细
export function getTeamstation(id) {
  return request({
    url: '/basic/teamstation/' + id,
    method: 'get'
  })
}

// 新增班组配置站点信息
export function addTeamstation(data) {
  return request({
    url: '/basic/teamstation',
    method: 'post',
    data: data
  })
}

// 修改班组配置站点信息
export function updateTeamstation(data) {
  return request({
    url: '/basic/teamstation',
    method: 'put',
    data: data
  })
}

// 删除班组配置站点信息
export function delTeamstation(id) {
  return request({
    url: '/basic/teamstation/' + id,
    method: 'delete'
  })
}
