import request from '@/utils/request'

// 查询大屏基础数据列表
export function listScreeninfo(query) {
  return request({
    url: '/basic/screeninfo/list',
    method: 'get',
    params: query
  })
}

// 查询大屏基础数据详细
export function getScreeninfo(id) {
  return request({
    url: '/basic/screeninfo/' + id,
    method: 'get'
  })
}

// 新增大屏基础数据
export function addScreeninfo(data) {
  return request({
    url: '/basic/screeninfo',
    method: 'post',
    data: data
  })
}

// 修改大屏基础数据
export function updateScreeninfo(data) {
  return request({
    url: '/basic/screeninfo',
    method: 'put',
    data: data
  })
}

// 删除大屏基础数据
export function delScreeninfo(id) {
  return request({
    url: '/basic/screeninfo/' + id,
    method: 'delete'
  })
}
