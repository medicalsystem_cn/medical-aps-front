import request from '@/utils/request'

// 查询移动端按钮列表
export function listButtoninfo(query) {
  return request({
    url: '/basic/buttoninfo/list',
    method: 'get',
    params: query
  })
}

// 查询移动端按钮详细
export function getButtoninfo(id) {
  return request({
    url: '/basic/buttoninfo/' + id,
    method: 'get'
  })
}

// 新增移动端按钮
export function addButtoninfo(data) {
  return request({
    url: '/basic/buttoninfo',
    method: 'post',
    data: data
  })
}

// 修改移动端按钮
export function updateButtoninfo(data) {
  return request({
    url: '/basic/buttoninfo',
    method: 'put',
    data: data
  })
}

// 删除移动端按钮
export function delButtoninfo(id) {
  return request({
    url: '/basic/buttoninfo/' + id,
    method: 'delete'
  })
}
