import request from '@/utils/request'

// 查询站点类型配置列表
export function listStationtype(query) {
  return request({
    url: '/basic/stationtype/list',
    method: 'get',
    params: query
  })
}

// 查询站点类型配置详细
export function getStationtype(id) {
  return request({
    url: '/basic/stationtype/' + id,
    method: 'get'
  })
}

// 新增站点类型配置
export function addStationtype(data) {
  return request({
    url: '/basic/stationtype',
    method: 'post',
    data: data
  })
}

// 修改站点类型配置
export function updateStationtype(data) {
  return request({
    url: '/basic/stationtype',
    method: 'put',
    data: data
  })
}

// 删除站点类型配置
export function delStationtype(id) {
  return request({
    url: '/basic/stationtype/' + id,
    method: 'delete'
  })
}
