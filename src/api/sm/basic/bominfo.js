import request from '@/utils/request'

// 查询物料Bom列表
export function listBominfo(query) {
  return request({
    url: '/basic/bominfo/list',
    method: 'get',
    params: query
  })
}

export function listAllBominfo(query) {
  return request({
    url: '/basic/bominfo/listall',
    method: 'get',
    params: query
  })
}

export function listChildren(query) {
  return request({
    url: '/basic/bominfo/listChildren',
    method: 'get',
    params: query
  })
}

export function listChildrenContainsStore(query) {
  return request({
    url: '/basic/bominfo/listChildren',
    method: 'get',
    params: query
  })
}





// 查询物料Bom详细
export function getBominfo(id) {
  return request({
    url: '/basic/bominfo/' + id,
    method: 'get'
  })
}

// 新增物料Bom
export function addBominfo(data) {
  return request({
    url: '/basic/bominfo',
    method: 'post',
    data: data
  })
}

// 修改物料Bom
export function updateBominfo(data) {
  return request({
    url: '/basic/bominfo',
    method: 'put',
    data: data
  })
}

// 删除物料Bom
export function delBominfo(id) {
  return request({
    url: '/basic/bominfo/' + id,
    method: 'delete'
  })
}

/**递归查询id下的所有物料信息列表*/
export function getRecBomInfoListByParentBomCode(parentBomcode) {
  return request({
    url: '/basic/bominfo/recBomInfoByCode/' + parentBomcode,
    method: 'get'
  })
}

/**递归查询id下的所有物料信息列表*/
export function getRecBomInfoListByParentBomId(parentBomCode) {
  return request({
    url: '/basic/bominfo/recBomInfoById/' + parentBomCode,
    method: 'get'
  })
}

/**递归查询id下的所有物料信息列表*/
export function listFactoryProductBom(factoryId) {
  return request({
    url: '/basic/bominfo/listFactoryProductBom/' + factoryId,
    method: 'get'
  })
}





// 查询订单信息详细
export function syncBom() {
  return request({
    url: '/basic/bominfo/syncBom',
    method: 'get'
  })
}

// 同步单个物料Bom列表
export function syncSingleBom(productId,bomCode) {
  return request({
    url: '/basic/bominfo/syncSingleBom/'+productId+'/'+ bomCode,
    method: 'get'
  })
}



