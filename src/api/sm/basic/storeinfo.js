import request from '@/utils/request'

// 查询仓库信息列表
export function listStoreinfo(query) {
  return request({
    url: '/basic/storeinfo/list',
    method: 'get',
    params: query
  })
}

// 查询仓库信息详细
export function getStoreinfo(id) {
  return request({
    url: '/basic/storeinfo/' + id,
    method: 'get'
  })
}

// 新增仓库信息
export function addStoreinfo(data) {
  return request({
    url: '/basic/storeinfo',
    method: 'post',
    data: data
  })
}

// 修改仓库信息
export function updateStoreinfo(data) {
  return request({
    url: '/basic/storeinfo',
    method: 'put',
    data: data
  })
}

// 删除仓库信息
export function delStoreinfo(id) {
  return request({
    url: '/basic/storeinfo/' + id,
    method: 'delete'
  })
}


// 查询仓库信息详细
export function syncStoreinfo() {
  return request({
    url: '/basic/storeinfo/sync',
    method: 'get'
  })
}
