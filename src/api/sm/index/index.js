import request from '@/utils/request'

// 查询首页
export function listIndexData(params) {
  return request({
    url: '/index/getData',
    method: 'post',
    data: params
  })
}
// 查询首页2
export function selectTodayProdList(params) {
  return request({
    url: '/order/statistics/selectTodayProdList',
    method: 'post',
    data: params
  })
}
// 查询首页3
export function selectDelayOrderList(params) {
  return request({
    url: '/order/statistics/selectDelayOrderList',
    method: 'post',
    data: params
  })
}
// 查询首页4
export function selectPurchaseStage(params) {
  return request({
    url: '/order/statistics/selectPurchaseStage',
    method: 'post',
    data: params
  })
}



// 初始化数据
export function iniData(query) {
  return request({
    url: '/index/iniData',
    method: 'get',
    params: query
  })
}





