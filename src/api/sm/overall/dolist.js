import request from '@/utils/request'

// 查询待办事项列表
export function listDolist(query) {
  return request({
    url: '/overall/dolist/list',
    method: 'get',
    params: query
  })
}

// 查询待办事项详细
export function getDolist(id) {
  return request({
    url: '/overall/dolist/' + id,
    method: 'get'
  })
}

// 新增待办事项
export function addDolist(data) {
  return request({
    url: '/overall/dolist',
    method: 'post',
    data: data
  })
}

// 修改待办事项
export function updateDolist(data) {
  return request({
    url: '/overall/dolist',
    method: 'put',
    data: data
  })
}

// 删除待办事项
export function delDolist(id) {
  return request({
    url: '/overall/dolist/' + id,
    method: 'delete'
  })
}
