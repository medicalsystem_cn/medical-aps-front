import request from '@/utils/request'

// 查询订单排产明细列表
export function listPlandetail(query) {
  return request({
    url: '/order/plandetail/list',
    method: 'get',
    params: query
  })
}

// 查询订单排产明细详细
export function getPlandetail(id) {
  return request({
    url: '/order/plandetail/' + id,
    method: 'get'
  })
}

// 新增订单排产明细
export function addPlandetail(data) {
  return request({
    url: '/order/plandetail',
    method: 'post',
    data: data
  })
}

// 修改订单排产明细
export function updatePlandetail(data) {
  return request({
    url: '/order/plandetail',
    method: 'put',
    data: data
  })
}

// 删除订单排产明细
export function delPlandetail(id) {
  return request({
    url: '/order/plandetail/' + id,
    method: 'delete'
  })
}
