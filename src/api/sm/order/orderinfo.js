import request from '@/utils/request'

// 查询订单信息列表
export function listOrderinfo(query) {
  return request({
    url: '/order/orderinfo/list',
    method: 'get',
    params: query
  })
}

// 查询订单信息列表
export function listFOrderinfo(query) {
  return request({
    url: '/order/orderinfo/listf',
    method: 'get',
    params: query
  })
}


// 查询订单信息列表
export function listPlanOrderinfo(query) {
  return request({
    url: '/order/orderinfo/planlist',
    method: 'get',
    params: query
  })
}


// 查询订单信息详细
export function getOrderinfo(id) {
  return request({
    url: '/order/orderinfo/' + id,
    method: 'get'
  })
}
// 查询订单信息详细
export function getInfoByOrderCode(orderCode) {
  return request({
    url: '/order/orderinfo/getInfoByOrderCode/' + orderCode,
    method: 'get'
  })
}

// 新增订单信息
export function addOrderinfo(data) {
  return request({
    url: '/order/orderinfo',
    method: 'post',
    data: data
  })
}

// 修改订单信息
export function updateOrderinfo(data) {
  return request({
    url: '/order/orderinfo',
    method: 'put',
    data: data
  })
}

// 删除订单信息
export function delOrderinfo(id) {
  return request({
    url: '/order/orderinfo/' + id,
    method: 'delete'
  })
}

// 批量创建销售订单信息
export function createSailOrderinfo(id) {
  return request({
    url: '/order/orderinfo/sailOrder/' + id,
    method: 'delete'
  })
}


// 查询订单信息详细
export function syncOrder() {
  return request({
    url: '/order/orderinfo/syncOrder',
    method: 'get'
  })
}

// 查询订单信息详细
export function syncOneOrder(id) {
  return request({
    url: '/order/orderinfo/syncOneOrder/'+id,
    method: 'get'
  })
}



// 总部订单取消
export function headOrderCancel(data) {
  return request({
    url: '/order/cancel/headOrder',
    method: 'post',
    data: data
  })
}


// 总部排产取消
export function planCancel(data) {
  return request({
    url: '/order/cancel/plan',
    method: 'post',
    data: data
  })
}


// 新的总部排产取消
export function newplanCancel(data) {
  return request({
    url: '/order/cancel/newplan',
    method: 'post',
    data: data
  })
}


// 打印领料单
export function printOrder(query) {
  return request({
    url: `/order/orderinfo/printOrder/${query.orderCode}`,
    method: 'get',
    params: query,
    responseType: 'blob'
  })
}


