import request from '@/utils/request'

// 查询产品出库列表
export function listOutbound(query) {
  return request({
    url: '/order/outbound/list',
    method: 'get',
    params: query
  })
}

// 查询产品出库详细
export function getOutbound(id) {
  return request({
    url: '/order/outbound/' + id,
    method: 'get'
  })
}

// 新增产品出库
export function addOutbound(data) {
  return request({
    url: '/order/outbound',
    method: 'post',
    data: data
  })
}

// 修改产品出库
export function updateOutbound(data) {
  return request({
    url: '/order/outbound',
    method: 'put',
    data: data
  })
}

// 删除产品出库
export function delOutbound(id) {
  return request({
    url: '/order/outbound/' + id,
    method: 'delete'
  })
}
