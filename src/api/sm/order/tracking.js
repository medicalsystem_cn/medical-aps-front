import request from '@/utils/request'

// 单个接口请求
export function orderTrackViewByCode(queryCode, data) {
  return request({
    url: `/orderTrackViewByCode/{queryCode}`,
    method: 'post',
    data: data
  })
}

// 多个接口并发请求
export function orderTrackViewByQueryCodes(data) {
  return request({
    url: `/orderTrackViewByQueryCodes/`,
    method: 'post',
    data: data
  })
}



