import request from '@/utils/request'

// 查询订单工单信息列表
export function listWorkorder(query) {
  return request({
    url: '/order/workorder/list',
    method: 'get',
    params: query
  })
}
// #$# 按照「产品系列分组」查询订单工单信息列表
export function querySeriesGroupList(query) {
  return request({
    url: '/order/workorder/querySeriesGroupList',
    method: 'get',
    params: query
  })
}
// #$# 按照「产品系列分组」查询订单工单信息列表
export function querySeriesGroupChildList(query) {
  return request({
    url: '/order/workorder/querySeriesGroupChildList',
    method: 'get',
    params: query
  })
}

// 查询订单工单信息列表
export function listMFactoryWorkorder(query) {
  return request({
    url: '/order/workorder/mfactorylist',
    method: 'get',
    params: query
  })
}

// 查询订单工单信息列表
export function listWFactoryWorkorder(query) {
  return request({
    url: '/order/workorder/wfactorylist',
    method: 'get',
    params: query
  })
}

// 现在的月排产执行排产
export function listFactoryMplanWorkorder(query) {
  return request({
    url: '/order/workorder/mplanlist',
    method: 'get',
    params: query
  })
}

// 符合条件批量月排产执行排产
export function listFactoryMplanWorkorderOfAll(query) {
  return request({
    url: '/order/workorder/listFactoryMplanWorkorderOfAll',
    method: 'get',
    params: query
  })
}

// 查询订单工单信息列表
export function listWplanWorkorder(query) {
  return request({
    url: '/order/workorder/wplanlist',
    method: 'get',
    params: query
  })
}

// 执行排产 选择的
export function listChooseWplanWorkorder(query) {
  return request({
    url: '/order/workorder/wplanchooselist',
    method: 'get',
    params: query
  })
}

// 执行排产 所有的
export function listChooseWplanWorkorderOfAll(query) {
  return request({
    url: '/order/workorder/listChooseWplanWorkorderOfAll',
    method: 'get',
    params: query
  })
}

export function listChooseWplanWorkorder4Cancel(query) {
  return request({
    url: '/order/workorder/wplanchooselist4Cancel',
    method: 'get',
    params: query
  })
}

export function listFactoryMplanWorkorder4Cancel(query) {
  return request({
    url: '/order/workorder/mplanlist4Cancel',
    method: 'get',
    params: query
  })
}

// 查询订单工单信息列表
export function listWfeedWorkorder(query) {
  return request({
    url: '/order/workorder/wfeedlist',
    method: 'get',
    params: query
  })
}
// 查询订单工单信息列表
export function listChooseWfeedWorkorder(query) {
  return request({
    url: '/order/workorder/wfeedchooselist',
    method: 'get',
    params: query
  })
}




// 查询订单工单信息详细
export function getWorkorder(id) {
  return request({
    url: '/order/workorder/' + id,
    method: 'get'
  })
}

// 新增订单工单信息
export function addWorkorder(data) {
  return request({
    url: '/order/workorder',
    method: 'post',
    data: data
  })
}

// 修改订单工单信息
export function updateWorkorder(data) {
  return request({
    url: '/order/workorder',
    method: 'put',
    data: data
  })
}

// 删除订单工单信息
export function delWorkorder(id) {
  return request({
    url: '/order/workorder/' + id,
    method: 'delete'
  })
}


// 批量接收工单
export function receiveWorkOrder(id) {
  return request({
    url: '/order/workorder/receiveWorkOrder/' + id,
    method: 'get'
  })
}

// 批量驳回工单
export function returnWorkOrder(id) {
  return request({
    url: '/order/workorder/returnWorkOrder/' + id,
    method: 'get'
  })
}

// 提交分单
export function submitWorkerOrder(data) {
  return request({
    url: '/order/workorder/submitWorkerOrder',
    method: 'post',
    data: data
  })
}


export function checkProductNumStore(id) {
  return request({
    url: '/order/workorder/checkProductNumStore/' + id,
    method: 'get'
  })
}

// 打印工单
export function printWorkOrder(id) {
  return request({
    url: '/order/workorder/printWorkOrder/' + id,
    method: 'get',
    responseType: 'blob'
  })
}
// 打印工单
export function printFactoryWorkorder(query) {
  return request({
    url: '/order/workorder/printFactoryWorkorder',
    method: 'get',
    params: query,
    responseType: 'blob'
  })
}


// mfactoryCanellist
export function getMfactoryCanellist(data) {
  return request({
    url: '/order/workorder/mfactoryCanellist',
    method: 'get',
    params: data
  })
}

// wfactoryCancellist
export function getWfactoryCancellist(data) {
  return request({
    url: '/order/workorder/wfactoryCancellist',
    method: 'get',
    data: data
  })
}

// 分单取消
export function splitCancel(data) {
  return request({
    url: '/order/cancel/split',
    method: 'post',
    data: data
  })
}

// 分厂月排产取消
export function monthPlanCancel(data) {
  return request({
    url: '/order/cancel/monthPlan',
    method: 'post',
    data: data
  })
}

// 分厂周排产取消
export function weekPlanCancel(data) {
  return request({
    url: '/order/cancel/weekPlan',
    method: 'post',
    data: data
  })
}


// 分单计算前置
export function preCalculateWorkerOrder(id) {
  return request({
    url: '/order/workorder/preCalculateWorkerOrder/' + id,
    method: 'get'
  })
}
// 分单计算-批量，补课调整分单数量
export function calculateWorkerOrderList(id) {
  return request({
    url: '/order/workorder/calculateWorkerOrderList/' + id,
    method: 'get'
  })
}
// 分单计算-批量，且可调整分单数量+采样数量
export function calculateWorkerOrderListOfBatch(data) {
  return request({
    url: '/order/workorder/calculateWorkerOrderListOfBatch',
    method: 'post',
    data: data
  })
}
// 先同步BOM=>分单计算-批量，且可调整分单数量+采样数量
export function calculateWorkerOrderListOfBatchAndSyncBom(data) {
  return request({
    url: '/order/workorder/calculateWorkerOrderListOfBatchAndSyncBom',
    method: 'post',
    data: data
  })
}

// 批量提交分单
export function batchSubmitWorkerOrder(data) {
  return request({
    url: '/order/workorder/batchSubmitWorkerOrder',
    method: 'post',
    data: data
  })
}

// 批量提交分单「最新方法」用来替代上面的方法
export function batchSubmitWorkerOrderNew(data) {
  return request({
    url: '/order/workorder/batchSubmitWorkerOrderNew',
    method: 'post',
    data: data
  })
}

// 手工推金蝶
export function handlePushKingdee(id) {
  return request({
    url: '/order/workorder/handlePushKingdee/' + id,
    method: 'get'
  })
}

// 查询投料单列表
export function queryMaterialRequisitionList(query) {
  return request({
    url: '/order/workorder/queryMaterialRequisitionList',
    method: 'get',
    params: query
  })
}

