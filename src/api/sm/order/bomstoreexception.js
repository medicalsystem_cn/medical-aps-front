import request from '@/utils/request'

// 查询库存不足信息列表
export function listBomstoreexception(query) {
  return request({
    url: '/order/bomstoreexception/list',
    method: 'get',
    params: query
  })
}

// 查询库存不足信息详细
export function getBomstoreexception(id) {
  return request({
    url: '/order/bomstoreexception/' + id,
    method: 'get'
  })
}

// 新增库存不足信息
export function addBomstoreexception(data) {
  return request({
    url: '/order/bomstoreexception',
    method: 'post',
    data: data
  })
}

// 修改库存不足信息
export function updateBomstoreexception(data) {
  return request({
    url: '/order/bomstoreexception',
    method: 'put',
    data: data
  })
}

// 删除库存不足信息
export function delBomstoreexception(id) {
  return request({
    url: '/order/bomstoreexception/' + id,
    method: 'delete'
  })
}
