import request from '@/utils/request'

// 查询排产调整申报审核列表
export function listAdjustapply(query) {
  return request({
    url: '/adjust/adjustapply/list',
    method: 'get',
    params: query
  })
}

// 查询排产调整申报审核详细
export function getAdjustapply(id) {
  return request({
    url: '/adjust/adjustapply/' + id,
    method: 'get'
  })
}

// 新增排产调整申报审核
export function addAdjustapply(data) {
  return request({
    url: '/adjust/adjustapply',
    method: 'post',
    data: data
  })
}

// 修改排产调整申报审核
export function updateAdjustapply(data) {
  return request({
    url: '/adjust/adjustapply',
    method: 'put',
    data: data
  })
}

// 删除排产调整申报审核
export function delAdjustapply(id) {
  return request({
    url: '/adjust/adjustapply/' + id,
    method: 'delete'
  })
}
