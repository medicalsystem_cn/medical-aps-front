import request from '@/utils/request'

// 查询总部排产列表
export function listOrderplan(query) {
  return request({
    url: '/order/orderplan/list',
    method: 'get',
    params: query
  })
}

// 查询总部排产详细
export function getOrderplan(id) {
  return request({
    url: '/order/orderplan/' + id,
    method: 'get'
  })
}

// 新增总部排产
export function addOrderplan(data) {
  return request({
    url: '/order/orderplan',
    method: 'post',
    data: data
  })
}

// 修改总部排产
export function updateOrderplan(data) {
  return request({
    url: '/order/orderplan',
    method: 'put',
    data: data
  })
}

// 删除总部排产
export function delOrderplan(id) {
  return request({
    url: '/order/orderplan/' + id,
    method: 'delete'
  })
}

// 执行排产
export function excuteOrderPlan(data) {
  return request({
    url: '/order/orderplan/excutePlan',
    method: 'post',
    data: data
  })
}

// 提交排产
export function submitPlan(data) {
  return request({
    url: '/order/orderplan/submitPlan',
    method: 'post',
    data: data
  })
}


// 调整排产
export function adjustPlan(data) {
  return request({
    url: '/order/orderplan/adjustPlan',
    method: 'post',
    data: data
  })
}


// ##################new################
// ##查询待排产
export function newPendingList(query) {
  return request({
    url: '/order/newplan/pendingList',
    method: 'get',
    params: query
  })
}
// ##查询待排产 - 分组查询+产能统计
export function pendingGroupList(query) {
  return request({
    url: '/order/newplan/pendingGroupList',
    method: 'get',
    params: query
  })
}
// ##查询已排产 - 分组查询+产能统计
export function doneGroupList(query) {
  return request({
    url: '/order/newplan/doneGroupList',
    method: 'get',
    params: query
  })
}

// ##查询待排产 - 分组查询+产能统计
export function pendingGroupListOfAll(query) {
  return request({
    url: '/order/newplan/pendingGroupListOfAll',
    method: 'get',
    params: query
  })
}

// ##查询已排产
export function newFinishList(query) {
  return request({
    url: '/order/newplan/finishList',
    method: 'get',
    params: query
  })
}

// #$# 按照「产品系列分组」查询订单工单信息列表
export function queryFinishSeriesGroupList(query) {
  return request({
    url: '/order/newplan/queryFinishSeriesGroupList',
    method: 'get',
    params: query
  })
}
// #$# 按照「产品系列分组」查询订单工单信息列表
export function queryFinishSeriesGroupChildList(query) {
  return request({
    url: '/order/newplan/queryFinishSeriesGroupChildList',
    method: 'get',
    params: query
  })
}



// 新产品排产计算
export function excuteNewPlan(data) {
  return request({
    url: '/order/newplan/excuteNewPlan',
    method: 'post',
    data: data
  })
}

// 新产品排产提交
export function submitNewPlan(data) {
  return request({
    url: '/order/newplan/submitNewPlan',
    method: 'post',
    data: data
  })
}


// 新产品排产调整
export function adjustNewPlan(data) {
  return request({
    url: '/order/newplan/adjustNewPlan',
    method: 'post',
    data: data
  })
}

// 新产品排产调整提交
export function submitAdjustNewPlan(data) {
  return request({
    url: '/order/newplan/submitAdjustNewPlan',
    method: 'post',
    data: data
  })
}

// 大盘查询
// ##查询已排产
export function planBigPanList(query) {
  return request({
    url: '/order/newplan/planBigPanList',
    method: 'get',
    params: query
  })
}

// 大盘按照班组分组查询
// ##大盘按照班组分组查询
export function bigPanListOfAll(query) {
  return request({
    url: '/order/newplan/bigPanListOfAll',
    method: 'get',
    params: query
  })
}

export function printPlanReport(query) {
  return request({
    url: '/order/newplan/printPlanReport',
    method: 'get',
    responseType: 'blob',
    params: query
  })
}


// ##查询已排产待领料的工单
export function newAdjustList(query) {
  return request({
    url: '/order/newplan/adjustList',
    method: 'get',
    params: query
  })
}


// ##查询已排产待领料的工单
export function queryHistoryAdjustList(query) {
  return request({
    url: '/adjust/adjusthis/list',
    method: 'get',
    params: query
  })
}


// TODO ##查询待调整的排产列表数据
export function queryAdjustPlanList(query) {
  return request({
    url: '/order/newplanV2/queryAdjustPlanList',
    method: 'get',
    params: query
  })
}

// TODO 批量调整排产计算（支持往前往后调整）
export function excuteNewV2Plan(data) {
  return request({
    url: '/order/newplanV2/excuteNewV2Plan',
    method: 'post',
    data: data
  })
}

// TODO 批量调整排产计算（支持往前往后调整）
export function submitNewV2Plan(data) {
  return request({
    url: '/order/newplanV2/submitNewV2Plan',
    method: 'post',
    data: data
  })
}




