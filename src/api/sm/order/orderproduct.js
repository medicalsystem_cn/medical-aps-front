import request from '@/utils/request'

// 查询订单产品信息列表
export function listOrderproduct(query) {
  return request({
    url: '/order/orderproduct/list',
    method: 'get',
    params: query
  })
}

// 查询订单产品信息详细
export function getOrderproduct(id) {
  return request({
    url: '/order/orderproduct/' + id,
    method: 'get'
  })
}

// 新增订单产品信息
export function addOrderproduct(data) {
  return request({
    url: '/order/orderproduct',
    method: 'post',
    data: data
  })
}

// 修改订单产品信息
export function updateOrderproduct(data) {
  return request({
    url: '/order/orderproduct',
    method: 'put',
    data: data
  })
}

// 删除订单产品信息
export function delOrderproduct(id) {
  return request({
    url: '/order/orderproduct/' + id,
    method: 'delete'
  })
}

// 查询订单信息列表
export function listSeparateOrderproduct(query) {
  return request({
    url: '/order/orderproduct/separateorderproductlist',
    method: 'get',
    params: query
  })
}

// 查询订单产品信息详细
export function getWorkerOrderproduct(id) {
  return request({
    url: '/order/orderproduct/workorderproduct/' + id,
    method: 'get'
  })
}

// 查询一级物料Bom列表
export function listProductbom(productId) {
  return request({
    url: '/order/orderproduct/productbomlist/' + productId,
    method: 'get'
  })
}  

// 
export function printProduct(id) {
  return request({
    url: '/order/orderproduct/printProduct/' + id,
    method: 'get',
    responseType: 'blob'
  })
}

export function listChildren(query) {
  return request({
    url: '/order/orderproduct/listChildren',
    method: 'get',
    params: query
  })
}


