import request from '@/utils/request'

// 查询分厂周投料计划列表
export function listFactorywfeed(query) {
  return request({
    url: '/factory/factorywfeed/list',
    method: 'get',
    params: query
  })
}

// 查询分厂周投料计划详细
export function getFactorywfeed(id) {
  return request({
    url: '/factory/factorywfeed/' + id,
    method: 'get'
  })
}

// 新增分厂周投料计划
export function addFactorywfeed(data) {
  return request({
    url: '/factory/factorywfeed',
    method: 'post',
    data: data
  })
}

// 修改分厂周投料计划
export function updateFactorywfeed(data) {
  return request({
    url: '/factory/factorywfeed',
    method: 'put',
    data: data
  })
}

// 删除分厂周投料计划
export function delFactorywfeed(id) {
  return request({
    url: '/factory/factorywfeed/' + id,
    method: 'delete'
  })
}


// 执行排产
export function excuteFactoryWFeed(data) {
  return request({
    url: '/factory/factorywfeed/excuteFactoryWFeed',
    method: 'post',
    data: data
  })
}

// 执行排产
export function submitFactoryWFeed(data) {
  return request({
    url: '/factory/factorywfeed/submitFactoryWFeed',
    method: 'post',
    data: data
  })
}
