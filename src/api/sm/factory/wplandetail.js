import request from '@/utils/request'

// 查询分厂周排产明细列表
export function listFactorywplandetail(query) {
  return request({
    url: '/factory/factorywplandetail/list',
    method: 'get',
    params: query
  })
}

// 查询分厂周排产明细列表
export function listWplandetailList(query) {
  return request({
    url: '/factory/factorywplandetail/detaillist',
    method: 'get',
    params: query
  })
}


// 查询分厂周排产明细详细
export function getFactorywplandetail(id) {
  return request({
    url: '/factory/factorywplandetail/' + id,
    method: 'get'
  })
}

// 新增分厂周排产明细
export function addFactorywplandetail(data) {
  return request({
    url: '/factory/factorywplandetail',
    method: 'post',
    data: data
  })
}

// 修改分厂周排产明细
export function updateFactorywplandetail(data) {
  return request({
    url: '/factory/factorywplandetail',
    method: 'put',
    data: data
  })
}

// 删除分厂周排产明细
export function delFactorywplandetail(id) {
  return request({
    url: '/factory/factorywplandetail/' + id,
    method: 'delete'
  })
}
