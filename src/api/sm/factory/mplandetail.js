import request from '@/utils/request'

// 查询分厂月排产明细列表
export function listMplandetail(query) {
  return request({
    url: '/factory/mplandetail/list',
    method: 'get',
    params: query
  })
}

// 查询分厂月排产明细详细
export function getMplandetail(id) {
  return request({
    url: '/factory/mplandetail/' + id,
    method: 'get'
  })
}

// 新增分厂月排产明细
export function addMplandetail(data) {
  return request({
    url: '/factory/mplandetail',
    method: 'post',
    data: data
  })
}

// 修改分厂月排产明细
export function updateMplandetail(data) {
  return request({
    url: '/factory/mplandetail',
    method: 'put',
    data: data
  })
}

// 删除分厂月排产明细
export function delMplandetail(id) {
  return request({
    url: '/factory/mplandetail/' + id,
    method: 'delete'
  })
}
