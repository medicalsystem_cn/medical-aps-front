import request from '@/utils/request'

// 查询分厂周排产列表
export function listFactorywplan(query) {
  return request({
    url: '/factory/factorywplan/list',
    method: 'get',
    params: query
  })
}

// 查询分厂周排产详细
export function getFactorywplan(id) {
  return request({
    url: '/factory/factorywplan/' + id,
    method: 'get'
  })
}

// 新增分厂周排产
export function addFactorywplan(data) {
  return request({
    url: '/factory/factorywplan',
    method: 'post',
    data: data
  })
}

// 修改分厂周排产
export function updateFactorywplan(data) {
  return request({
    url: '/factory/factorywplan',
    method: 'put',
    data: data
  })
}

// 删除分厂周排产
export function delFactorywplan(id) {
  return request({
    url: '/factory/factorywplan/' + id,
    method: 'delete'
  })
}


// 执行排产
export function excuteFactoryWPlan(data) {
  return request({
    url: '/factory/factorywplan/excuteFactoryWPlan',
    method: 'post',
    data: data
  })
}

// 执行排产
export function submitFactoryWPlan(data) {
  return request({
    url: '/factory/factorywplan/submitFactoryWPlan',
    method: 'post',
    data: data
  })
}

// 查询分厂周排产详细
export function printWplanWorkOrder(id) {
  return request({
    url: '/factory/factorywplan/printWplanWorkOrder/' + id,
    method: 'get',
    responseType: 'blob'
  })
}

// 查询分厂周排产详细
export function printPlan(data) {
  return request({
    url: '/factory/factorywplan/printWPlan',
    method: 'post',
    data: data,
    responseType: 'blob'
  })
}


// 查询分厂班组周排产列表
export function listFactoryPrintwplan(query) {
  return request({
    url: '/factory/factorywplan/wplanprintlist',
    method: 'get',
    params: query
  })
}

// 查询分厂周排产详细
export function printFactorywplan(query) {
  return request({
    url: '/factory/factorywplan/printFactorywplan',
    method: 'get',
    params: query,
    responseType: 'blob'
  })
}

// 调整周排产
export function adjustWplan(data) {
  return request({
    url: '/factory/factorywplan/adjustWplan',
    method: 'post',
    data: data
  })
}

export function getWeekOfMonth(startDate) {
  return request({
    url: `/order/statistics/getWeekOfMonth/${startDate}`,
    method: 'get'
  })
}


// 查询分厂周排产大盘视图
export function weekViewList(data) {
  return request({
    url: '/factory/factorywplandetail/viewlist',
    method: 'post',
    data: data
  })
}


