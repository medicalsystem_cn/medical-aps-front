import request from '@/utils/request'

// 查询分厂月排产列表
export function listFactorymplan(query) {
  return request({
    url: '/factory/factorymplan/list',
    method: 'get',
    params: query
  })
}

// 查询分厂月排产详细
export function getFactorymplan(id) {
  return request({
    url: '/factory/factorymplan/' + id,
    method: 'get'
  })
}

// 查询分厂月排产详细
export function printMplanWorkOrder(id) {
  return request({
    url: '/factory/factorymplan/printMplanWorkOrder/' + id,
    method: 'get',
    responseType: 'blob'
  })
}

// 查询分厂月排产详细
export function printMplanBom(month,teamId) {
  return request({
    url: '/factory/factorymplan/printMplanBom/' + month+'/'+teamId,
    method: 'get',
    responseType: 'blob'
  })
}


// 查询分厂月排产详细
export function teamMplanBomList(month,teamId) {
  return request({
    url: '/factory/factorymplan/teamMplanBomList/' + month+'/'+teamId,
    method: 'get'
  })
}



// 新增分厂月排产
export function addFactorymplan(data) {
  return request({
    url: '/factory/factorymplan',
    method: 'post',
    data: data
  })
}

// 修改分厂月排产
export function updateFactorymplan(data) {
  return request({
    url: '/factory/factorymplan',
    method: 'put',
    data: data
  })
}

// 删除分厂月排产
export function delFactorymplan(id) {
  return request({
    url: '/factory/factorymplan/' + id,
    method: 'delete'
  })
}

// 执行排产
export function excuteFactoryMPlan(data) {
  return request({
    url: '/factory/factorymplan/excuteFactoryMPlan',
    method: 'post',
    data: data
  })
}

// 执行排产
export function submitFactoryMPlan(data) {
  return request({
    url: '/factory/factorymplan/submitFactoryMPlan',
    method: 'post',
    data: data
  })
}

// 排产大盘视图 /factory/mplandetail/viewlist
export function planBigpanList(data) {
  return request({
    url: '/factory/mplandetail/viewlist',
    method: 'post',
    data: data
  })
}

// #排产大盘视图（月）# /factory/mplandetail/viewlist_month
export function planviewlist_month(data) {
  return request({
    url: '/factory/mplandetail/viewlist_month',
    method: 'post',
    data: data
  })
}
