import request from '@/utils/request'

// 查询分厂周投料计划明细列表
export function listWfeeddetail(query) {
  return request({
    url: '/factory/wfeeddetail/list',
    method: 'get',
    params: query
  })
}

// 查询分厂周投料计划明细详细
export function getWfeeddetail(id) {
  return request({
    url: '/factory/wfeeddetail/' + id,
    method: 'get'
  })
}

// 新增分厂周投料计划明细
export function addWfeeddetail(data) {
  return request({
    url: '/factory/wfeeddetail',
    method: 'post',
    data: data
  })
}

// 修改分厂周投料计划明细
export function updateWfeeddetail(data) {
  return request({
    url: '/factory/wfeeddetail',
    method: 'put',
    data: data
  })
}

// 删除分厂周投料计划明细
export function delWfeeddetail(id) {
  return request({
    url: '/factory/wfeeddetail/' + id,
    method: 'delete'
  })
}
