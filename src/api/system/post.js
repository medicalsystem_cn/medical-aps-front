import request from "@/utils/request";

// 查询岗位列表
export function listPost(query) {
  return request({
    url: "/system/post/list",
    method: "get",
    params: query,
  });
}

// 查询岗位详细
export function getPost(postId) {
  return request({
    url: "/system/post/" + postId,
    method: "get",
  });
}

// 新增岗位
export function addPost(data) {
  return request({
    url: "/system/post",
    method: "post",
    data: data,
  });
}

// 修改岗位
export function updatePost(data) {
  return request({
    url: "/system/post",
    method: "put",
    data: data,
  });
}

// 删除岗位
export function delPost(postId) {
  return request({
    url: "/system/post/" + postId,
    method: "delete",
  });
}

// 统计接单情况
export function selectOrderReceive(postId) {
  return request({
    url: "/order/statistics/selectOrderReceive",
    method: "get",
    params: postId,
  });
}

// 统计生产
export function selectOrderProduce(postId) {
  return request({
    url: "/order/statistics/selectOrderProduce",
    method: "get",
    params: postId,
  });
}

// 统计发货情况
export function selectOrderShip(postId) {
  return request({
    url: "/order/statistics/selectOrderShip",
    method: "get",
    params: postId,
  });
}

// 统计库存异常情况
export function selectStoreException(postId) {
  return request({
    url: "/order/statistics/selectStoreException",
    method: "get",
    params: postId,
  });
}

// 统计库存完工情况
export function selectOrderFinish(postId) {
  return request({
    url: "/order/statistics/selectOrderFinish",
    method: "get",
    params: postId,
  });
}
// 统计延期情况
export function selectProduceException(postId) {
  return request({
    url: "/order/statistics/selectProduceException",
    method: "get",
    params: postId,
  });
}
// 完成情况
export function selectWorkerOrderFinish(postId) {
  return request({
    url: "/order/statistics/selectWorkerOrderFinish",
    method: "get",
    params: postId,
  });
}

// 成品班组产能情况
export function selectTeamGroupCapacity(postId) {
  return request({
    url: "/order/statistics/selectTeamGroupCapacity",
    method: "get",
    params: postId,
  });
}