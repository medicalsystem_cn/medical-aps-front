export const timeWhere = {
  data() {
    return {
      value1: [],
    };
  },
  methods: {
    changeTime(event, cbStr = "", isFlat = false) {
      if (isFlat) {
        this.value1 = [];
      }
      this.$emit("timeChanage", {
        funCb: cbStr,
        data_: isFlat ? "" : this.value1,
      });
    },
  },
};
